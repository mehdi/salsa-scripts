#!/bin/sh

set -e

. ./salsarc

if [ "$#" -ne 1 ] || [ -z "$1" ] ; then
  echo "Usage: $0 pkg-baz" >&2
  echo "  where 'pkg-baz' is the name of the Salsa group for which you want the project list" >&2
  exit 1
fi

SALSA_GROUP="$1"
PER_PAGE=100
HEADERS_FILE=$(mktemp)
trap "rm -f $HEADERS_FILE" EXIT

SALSA_GROUP_ID=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/groups?all_available=false" | jq ".[] | select(.path == \"$SALSA_GROUP\") | .id")

get_one_page ()
{
    curl --silent -D $HEADERS_FILE "$SALSA_URL/groups/$SALSA_GROUP_ID/projects?private_token=$SALSA_TOKEN&simple=true&per_page=$PER_PAGE&page=$PAGENO" | jq --raw-output ".[] | .path"
}

PAGENO=1

get_one_page

TOTAL_PAGES=$(grep ^X-Total-Pages: $HEADERS_FILE)
TOTAL_PAGES=${TOTAL_PAGES#*: }

while [ $PAGENO -lt $TOTAL_PAGES ]
do
    PAGENO=$((PAGENO+1))
    get_one_page
done
