#!/bin/bash

set -eu

. ./salsarc

if [ "$#" -ne 1 ] || [ -z "$1" ]; then
  echo "Usage: $0 pkg-bar/foo" >&2
  echo "  where 'pkg-bar/foo' is the Salsa group for which all projects should go to kgb" >&2
  exit 1
fi

PROJECT_NAME="$1"
PROJECT_PATH="${PROJECT_NAME//\//%2F}"
urlencode() {
    local LANG=C i c e=''
    for ((i=0;i<${#1};i++)); do
        c=${1:$i:1}
	[[ "$c" =~ [a-zA-Z0-9\.\~\_\-] ]] || printf -v c '%%%02X' "'$c"
        e+="$c"
    done
    echo "$e"
}

function dogroup() {
    local pgroup=${1:-}
    local subs=${2:-"n"}

    if [[ -z ${pgroup} ]]; then
        echo "Need a group to process"
        return
    fi

    if [[ ${subs} == "y" ]]; then
        while IFS=$'\t' read -r subid subname; do
            echo "Checking subgroup ${subid}: ${subname}"
            dogroup ${subid} y
        done < <(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/groups/${pgroup}/subgroups" | jq  -r '.[] | [.id, .name] | @tsv')
        echo "All subgroups done, now projects for ${pgroup}"
        while IFS=$'\t' read -r projid desc; do
            echo "Project ${projid}, ${desc}"
            while IFS=$'\t' read -r irkerid irkerchannel; do
                if [[ -z ${irkerchannel} ]]; then
                   continue
                fi
                echo "Got: $irkerid, $irkerchannel"
                echo "Irker integration for ${desc} exists, switching to KGB, channel ${irkerchannel}"

                irkerchannel=${irkerchannel//#/}
                url=$(urlencode "http://kgb.debian.net:9418/webhook/?channel=${irkerchannel}&network=oftc&private=1&use_color=1&use_irc_notices=1&squash_threshold=20")
                curl -XDELETE --header "PRIVATE-TOKEN: $SALSA_TOKEN" $SALSA_URL/projects/$projid/services/irker
	        curl -XPOST --header "PRIVATE-TOKEN: $SALSA_TOKEN" $SALSA_URL/projects/$projid/hooks \
	             --data "url=${url}&push_events=yes&issues_events=yes&merge_requests_events=yes&tag_push_events=yes&note_events=yes&job_events=yes&pipeline_events=yes&wiki_events=yes&enable_ssl_verification=yes"
            done < <(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/projects/${projid}/services/irker" | jq  -r '[.id, .properties.recipients] | @tsv')
        done < <(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/groups/${pgroup}/projects" | jq  -r '.[] | [.id, .description] | @tsv')
    fi
}

GROUP_ID=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/groups?search=${PROJECT_PATH}" | jq '.[] | .id')

dogroup ${GROUP_ID} y
